# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'loginwindow.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_loginWindow(object):
    def setupUi(self, loginWindow):
        loginWindow.setObjectName("loginWindow")
        loginWindow.resize(523, 300)
        loginWindow.setMinimumSize(QtCore.QSize(523, 300))
        loginWindow.setMaximumSize(QtCore.QSize(523, 300))
        loginWindow.setFocusPolicy(QtCore.Qt.ClickFocus)
        loginWindow.setAutoFillBackground(False)
        self.centralWidget = QtWidgets.QWidget(loginWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.welcomeLbl = QtWidgets.QLabel(self.centralWidget)
        self.welcomeLbl.setGeometry(QtCore.QRect(190, 30, 141, 58))
        self.welcomeLbl.setObjectName("welcomeLbl")
        self.loginButton = QtWidgets.QPushButton(self.centralWidget)
        self.loginButton.setGeometry(QtCore.QRect(210, 190, 91, 41))
        self.loginButton.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.loginButton.setObjectName("loginButton")
        self.usernameTxt = QtWidgets.QLineEdit(self.centralWidget)
        self.usernameTxt.setGeometry(QtCore.QRect(172, 100, 161, 31))
        self.usernameTxt.setText("")
        self.usernameTxt.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.usernameTxt.setAlignment(QtCore.Qt.AlignCenter)
        self.usernameTxt.setObjectName("usernameTxt")
        self.passwordTxt = QtWidgets.QLineEdit(self.centralWidget)
        self.passwordTxt.setGeometry(QtCore.QRect(172, 140, 161, 31))
        self.passwordTxt.setText("")
        self.passwordTxt.setEchoMode(QtWidgets.QLineEdit.Password)
        self.passwordTxt.setAlignment(QtCore.Qt.AlignCenter)
        self.passwordTxt.setObjectName("passwordTxt")
        loginWindow.setCentralWidget(self.centralWidget)
        self.mainToolBar = QtWidgets.QToolBar(loginWindow)
        self.mainToolBar.setObjectName("mainToolBar")
        loginWindow.addToolBar(QtCore.Qt.TopToolBarArea, self.mainToolBar)
        self.statusBar = QtWidgets.QStatusBar(loginWindow)
        self.statusBar.setObjectName("statusBar")
        loginWindow.setStatusBar(self.statusBar)
        self.menuBar = QtWidgets.QMenuBar(loginWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 523, 22))
        self.menuBar.setDefaultUp(False)
        self.menuBar.setNativeMenuBar(False)
        self.menuBar.setObjectName("menuBar")
        self.homeMenu = QtWidgets.QMenu(self.menuBar)
        self.homeMenu.setObjectName("homeMenu")
        self.menuAbout = QtWidgets.QMenu(self.menuBar)
        self.menuAbout.setObjectName("menuAbout")
        loginWindow.setMenuBar(self.menuBar)
        self.actionLogin = QtWidgets.QAction(loginWindow)
        self.actionLogin.setObjectName("actionLogin")
        self.actionExit = QtWidgets.QAction(loginWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionExit_2 = QtWidgets.QAction(loginWindow)
        self.actionExit_2.setObjectName("actionExit_2")
        self.actionCymon_Desktop = QtWidgets.QAction(loginWindow)
        self.actionCymon_Desktop.setObjectName("actionCymon_Desktop")
        self.actionDeveloper = QtWidgets.QAction(loginWindow)
        self.actionDeveloper.setObjectName("actionDeveloper")
        self.actionVersion = QtWidgets.QAction(loginWindow)
        self.actionVersion.setObjectName("actionVersion")
        self.homeMenu.addAction(self.actionLogin)
        self.homeMenu.addSeparator()
        self.homeMenu.addAction(self.actionExit_2)
        self.menuAbout.addAction(self.actionCymon_Desktop)
        self.menuAbout.addAction(self.actionDeveloper)
        self.menuAbout.addAction(self.actionVersion)
        self.menuBar.addAction(self.homeMenu.menuAction())
        self.menuBar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(loginWindow)
        QtCore.QMetaObject.connectSlotsByName(loginWindow)
        loginWindow.setTabOrder(self.loginButton, self.usernameTxt)
        loginWindow.setTabOrder(self.usernameTxt, self.passwordTxt)

    def retranslateUi(self, loginWindow):
        _translate = QtCore.QCoreApplication.translate
        loginWindow.setWindowTitle(_translate("loginWindow", "Login"))
        self.welcomeLbl.setText(_translate("loginWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt;\">Cymon Desktop</span></p><p align=\"center\"><span style=\" font-size:14pt;\">Welcome!</span></p></body></html>"))
        self.loginButton.setText(_translate("loginWindow", "Login"))
        self.usernameTxt.setPlaceholderText(_translate("loginWindow", "USERNAME"))
        self.passwordTxt.setPlaceholderText(_translate("loginWindow", "PASSWORD"))
        self.homeMenu.setTitle(_translate("loginWindow", "Home"))
        self.menuAbout.setTitle(_translate("loginWindow", "About"))
        self.actionLogin.setText(_translate("loginWindow", "Login"))
        self.actionExit.setText(_translate("loginWindow", "Exit"))
        self.actionExit_2.setText(_translate("loginWindow", "Exit"))
        self.actionCymon_Desktop.setText(_translate("loginWindow", "Cymon Desktop"))
        self.actionDeveloper.setText(_translate("loginWindow", "Developer"))
        self.actionVersion.setText(_translate("loginWindow", "Version"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    loginWindow = QtWidgets.QMainWindow()
    ui = Ui_loginWindow()
    ui.setupUi(loginWindow)
    loginWindow.show()
    sys.exit(app.exec_())

