from PyQt5.QtWidgets import QApplication, QMainWindow
from iocsubmitwindow import Ui_IoCsubmitWindow
import sys

class load_iocGUI(QMainWindow, Ui_IoCsubmitWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)

        self.menu = self.menuBar()
        self.menu.setNativeMenuBar(False)
        self.clearButton.clicked.connect(self.clearButton_clicked)
        self.submitButton.clicked.connect(self.submitButton_clicked)

    def clearButton_clicked(self):
        self.titeTxt.setText('')
        self.descriptTxt.setText('')
        self.linkTxt.setText('')
        self.tagsTxt.setText('')
        self.IPv4Txt.setText('')
        self.tagsTxt_3.setText('') #IPv6. needs to be changed in Qt desiner
        self.hostnameTxt.setText('')
        self.urlTXT.setText('')
        self.md5Txt.setText('')
        self.SHA1Txt.setText('')
        self.SHA256Txt.setText('')
        self.ssdeepTxt.setText('')

    def submitButton_clicked(self):
        pass

def main():
    App = QApplication(sys.argv)
    MainApp = load_iocGUI()
    MainApp.show()
    sys.exit(App.exec_())

if __name__ == '__main__': main()
