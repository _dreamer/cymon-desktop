#ifndef IOCSUBMITWINDOW_H
#define IOCSUBMITWINDOW_H

#include <QMainWindow>

namespace Ui {
class IoCsubmitWindow;
}

class IoCsubmitWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit IoCsubmitWindow(QWidget *parent = 0);
    ~IoCsubmitWindow();

private:
    Ui::IoCsubmitWindow *ui;
};

#endif // IOCSUBMITWINDOW_H
