#include "feedlistwindow.h"
#include "ui_feedlistwindow.h"

FeedListWindow::FeedListWindow(QWidget *parent) :
    QStackedWidget(parent),
    ui(new Ui::FeedListWindow)
{
    ui->setupUi(this);
}

FeedListWindow::~FeedListWindow()
{
    delete ui;
}
