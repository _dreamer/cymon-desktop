#include "iocsubmitwindow.h"
#include "ui_iocsubmitwindow.h"

IoCsubmitWindow::IoCsubmitWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::IoCsubmitWindow)
{
    ui->setupUi(this);
    menuBar()->setNativeMenuBar(false);
}

IoCsubmitWindow::~IoCsubmitWindow()
{
    delete ui;
}
