#-------------------------------------------------
#
# Project created by QtCreator 2016-07-04T15:52:19
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = desingFiles
TEMPLATE = app


SOURCES += main.cpp\
        loginwindow.cpp \
    iocsubmitwindow.cpp

HEADERS  += loginwindow.h \
    iocsubmitwindow.h

FORMS    += loginwindow.ui \
    iocsubmitwindow.ui
