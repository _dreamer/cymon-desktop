import requests
import time
from datetime import datetime

class Cymon(object):
    def __init__(self, username, password, endpoint='https://p061vjnim7.execute-api.us-east-1.amazonaws.com/dev'):
        self.endpoint = endpoint
        self.creds = {'username': username, 'password': password}
        self.status = None
        self.dic = {'token' : None, 'token_expiray' : None, 'status' : None}

    def login(self):
        h = {'Content-Type': 'application/json'}
        r = requests.post(self.endpoint + '/auth/login', json=self.creds, headers=h)
        #r.raise_for_status()
        self.dic['status'] = r.status_code
        self.dic['token'] = r.json().get('jwt')
        if r.status_code == 200:
            self.dic['token_expiry'] = datetime.fromtimestamp(r.json().get('expires'))
            
        return self.dic['token']

    def get(self, path):
        self.check_token()
        h = {'Authorization': 'Bearer {}'.format(self.dic['token'])}
        r = requests.get(self.endpoint + path, headers=h)
        r.raise_for_status()
        return r

    def post(self, path, params):
        self.check_token()
        h = {
            'Authorization': 'Bearer {}'.format(self.dic['token']),
            'Content-Type': 'application/json'
        }
        r = requests.post(self.endpoint + path, json=params, headers=h)
        r.raise_for_status()
        return r

    def check_token(self):
        if self.dic['token'] is None or self.dic['token_expiry'] is None:
            self.login()
        if datetime.utcnow() > self.dic['token_expiry']:
            self.login()
