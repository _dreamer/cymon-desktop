# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QApplication, QMainWindow
from loginwindow import Ui_loginWindow
from cymon_IoCSubmitPage import load_iocGUI
import cymon_API
import sys

class login_GUI(QMainWindow, Ui_loginWindow):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setupUi(self)

        self.IoCwindow = load_iocGUI(self)
        self.cymon = None

        self.actionExit_2.triggered.connect(self.exit_program)
        self.actionExit.triggered.connect(self.exit_program)
        self.actionLogin.triggered.connect(self.login_clicked)
        self.loginButton.clicked.connect(self.login_clicked)

    def login_clicked(self):
        self.cymon = cymon_API.Cymon(self.usernameTxt.text(), self.passwordTxt.text())
        self.cymon.login()

        if self.cymon.dic['status'] == 200:
            self.statusBar.showMessage('Login Successful!')
            self.load_IOCwindow()
        else:
             self.statusBar.showMessage('Login Failed. Try Again!')

    def load_IOCwindow(self):
        self.IoCwindow.show()

    def exit_program(self):
        QApplication.quit()

def main():
    App = QApplication(sys.argv)
    MainApp = login_GUI()
    MainApp.show()
    sys.exit(App.exec_())

if __name__ == '__main__': main()
